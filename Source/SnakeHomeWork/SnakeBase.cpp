// Fill out your copyright notice in the Description page of Project Settings.

#define print(text) if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::Green,text)
#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 0.5;
	LastMovementDirection = EMovementDirection::E_UP;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int Amount)
{
	for (int i = 0; i < Amount; i ++) {
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		auto ElementIndex = SnakeElements.Add(NewSnakeElement);
		if (ElementIndex == 0) 
		{
			NewSnakeElement->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector;
	float MovementSpeedDelta = ElementSize;

	switch (LastMovementDirection)
	{
	default:
	case EMovementDirection::E_UP:
		MovementVector = FVector(-MovementSpeedDelta, 0, 0);
		break;
	case EMovementDirection::E_DOWN:
		MovementVector = FVector(MovementSpeedDelta, 0, 0);
		break;
	case EMovementDirection::E_LEFT:
		MovementVector = FVector(0, MovementSpeedDelta, 0);
		break;
	case EMovementDirection::E_RIGHT:
		MovementVector = FVector(0, -MovementSpeedDelta, 0);
		break;
	}

	for (int i = SnakeElements.Num() - 1; i > 0; i--) {
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
}