// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM(BlueprintType)
enum class EMovementDirection : uint8
{
	E_UP UMETA(DisplayName="Up"),
	E_RIGHT UMETA(DisplayName="Right"),
	E_DOWN UMETA(DisplayName="Down"),
	E_LEFT UMETA(DisplayName="Left")
};

UCLASS()
class SNAKEHOMEWORK_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
	EMovementDirection LastMovementDirection;

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElement(int Amount = 1);

	void Move();
};
