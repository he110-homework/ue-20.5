// Copyright Epic Games, Inc. All Rights Reserved.

#include "SnakeHomeWork.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SnakeHomeWork, "SnakeHomeWork" );
