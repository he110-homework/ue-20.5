// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeHomeWorkGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEHOMEWORK_API ASnakeHomeWorkGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
